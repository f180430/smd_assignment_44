package com.ialimurtaza.smdassignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText etCity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendCityData(View view){
        etCity = findViewById(R.id.city);
        String textCity = etCity.getText().toString();
        if(TextUtils.isEmpty(textCity)){
            Toast.makeText(this, "City Name is required",Toast.LENGTH_LONG);

            etCity.setError("City Name is required to make it work!");
        }else{
            Intent i = new Intent(getApplicationContext(), WeatherAcitivity.class);
            i.putExtra("CityName", textCity);
            startActivity(i);
        }
    }

}