package com.ialimurtaza.smdassignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

public class WeatherAcitivity extends AppCompatActivity {

    TextView cityText = findViewById(R.id.city);
    TextView countryText = findViewById(R.id.country);
    TextView temperatureText = findViewById(R.id.temperature);
    TextView weatherDescriptions = findViewById(R.id.weather_descriptions);
    TextView windSpeed = findViewById(R.id.wind_speed);
    TextView windDir = findViewById(R.id.wind_dir);
    TextView precip = findViewById(R.id.presip);
    TextView humidity = findViewById(R.id.humidity);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_acitivity);
        String city = getIntent().getStringExtra("CityName");
        String apiUrl = "http://api.weatherstack.com/current?access_key=3ac6385e9ddfe336400fc206efc1f089&query="+city;
        //String apiUrl = "https://api.github.com/users";

        StringRequest requestData = new StringRequest(apiUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                WeatherData weatherData = gson.fromJson(response, WeatherData.class);
                cityText.setText(weatherData.getLocation().getName());
                countryText.setText(weatherData.getLocation().getCountry());
                temperatureText.setText(weatherData.getCurrent().getTemperature().toString());
                weatherDescriptions.setText(weatherData.getCurrent().getWeatherDescriptions().toString());
                windSpeed.setText(weatherData.getCurrent().getWindSpeed().toString());
                windDir.setText(weatherData.getCurrent().getWindDir());
                precip.setText(weatherData.getCurrent().getPrecip().toString());
                humidity.setText(weatherData.getCurrent().getHumidity().toString());










            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                TextView textView = findViewById(R.id.textView);
//                textView.setText(error.toString());

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestData);






    }
}